# beeminder-reporters

This is a monorepo for several scripts I've written that pipe data from various services
into [Beeminder](https://beeminder.com). General installation instructions follow; any specific changes are noted
in the sections for the specific reporter.

Each of these expect to be run by a timer: each individual execution is a "oneshot"
reporter.

If you're using Arch Linux or another distro that uses `pacman`, download the desired
latest release from gitlab (quick links below), extract it, and make the package:

- [jmap-beeminder-reporter v0.2.1](https://gitlab.com/rperce/beeminder-reporters/-/releases/jmap-v0.2.1/downloads/jmap-beeminder-reporter.tar.gz)
- [storygraph-beeminder-reporter v0.1.1](https://gitlab.com/rperce/beeminder-reporters/-/releases/storygraph-v0.1.1/downloads/storygraph-beeminder-reporter.tar.gz)

```
tar xzf <reporter>-beeminder-reporter.tar.gz
cd <reporter>-beeminder-reporter
makepkg -sri
```

On other systems, you'll need to manually move the `.cjs` into your `PATH`, the
`.service` and `.timer` to somewhere that systemd can see, and the `.toml` to
`/etc/conf.d/` (or, if you edit the path in the `.service` file, wherever you want).
That conf file will contain various passwords and tokens in plaintext, so
ensure it's chmodded `600` and owned by root.

If you prefer to build from source, your commands will take roughly the form of (after
installing a node version that matches the `engines` key in `package.json` and a global
`yarn` package),

```
yarn install
./Taskfile build <reporter>
./Taskfile package <reporter>
```

And then proceed as if you'd downloaded the .tar.gz, above.

## JMAP

Beeminder has a Gmail integration for creating inbox-zero style
goals. Unfortunately, they don't have integrations with any other providers.

I use Fastmail, which provides API access over [JMAP](https://jmap.io/), so I've written
this small script and a systemd service to automatically update my own [inbox-zero
goal](https://beeminder.com/rperce/inbox-zero).

### Building from source

The goal slug `inbox-zero` and the JMAP api url
`https://betajmap.fastmail.com/.well-known/jmap` are hardcoded. If you want a different
slug or have your email on something else that supports JMAP (there doesn't appear to be
much yet at time of writing), you'll need to build from source as above.

### Config

Edit `/etc/conf.d/beeminder-reporters` and insert your beeminder token and either
your JMAP username and password (for basic auth) or a bearer token (for Fastmail, use an
API token from Settings -> Account -> Password and Security -> API tokens -> Add).

import * as fs from 'fs'
import * as esbuild from 'esbuild'
esbuild.buildSync({
  entryPoints: fs
    .readdirSync('./src', { withFileTypes: true })
    .filter((dirent) => dirent.isFile() && dirent.name.includes('beeminder-reporter'))
    .map((file) => `src/${file.name}`),
  bundle: true,
  platform: 'node',
  target: ['node16'],
  outdir: 'dist',
  outExtension: { '.js': '.cjs' },
  banner: { js: '#!/usr/bin/env node' },
})

#!/usr/bin/env bash

cat <<EOF
[beeminder]
token="insert the token from https://www.beeminder.com/api/v1/auth_token.json"

# delete the sections you don't need
[jmap]
user="JMAP username"
pass="JMAP password"
token="Either set (user and pass) OR (token) but not both"

[storygraph]
user="thestorygraph.com username"
pass="thestorygraph.com password"
EOF

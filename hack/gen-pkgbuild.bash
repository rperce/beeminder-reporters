#!/usr/bin/env sh
pkgname="$1"-beeminder-reporter

pjs() { jq -r "$1" package.json; }
md5() { md5sum "$1" | cut -f1 -d\ ; }

# no preceding slash for `backup` on purpose
cat <<EOF
# Maintainer: $(pjs .author)
pkgname=${pkgname}
pkgver=$(pjs .version)
pkgrel=1
pkgdesc="$(pjs .description)"
arch=('any')
url="$(pjs .repository)"
license=('$(pjs .license)')
depends=('nodejs$(pjs .engines.node)')
provides=("${pkgname%-VCS}")
backup=('etc/conf.d/beeminder-reporters.toml') 
source=("\$pkgname.cjs"
        "\$pkgname.service"
        "\$pkgname.timer"
        "beeminder-reporters.toml"
)
md5sums=("$(md5 "${pkgname}/${pkgname}.cjs")"
         "$(md5 "${pkgname}/${pkgname}.service")"
         "$(md5 "${pkgname}/${pkgname}.timer")"
         "$(md5 "${pkgname}/beeminder-reporters.toml")"
)

package() {
  install -Dm755 "\${srcdir}/\${pkgname}.cjs" "\${pkgdir}/usr/bin/\${pkgname}.cjs"
  install -Dm644 "\${srcdir}/\${pkgname}.service" "\${pkgdir}/usr/lib/systemd/system/\${pkgname}.service"
  install -Dm644 "\${srcdir}/\${pkgname}.timer" "\${pkgdir}/usr/lib/systemd/system/\${pkgname}.timer"
  install -Dm600 "\${srcdir}/beeminder-reporters.toml" "\${pkgdir}/etc/conf.d/beeminder-reporters.toml"
}
EOF

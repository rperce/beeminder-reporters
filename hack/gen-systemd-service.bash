#!/usr/bin/env bash
source="$1"

cat <<EOF
[Unit]
Description=Script to sync data from ${source} to beeminder
After=network.target

[Service]
Type=oneshot
ExecStart=${source}-beeminder-reporter.cjs

[Install]
WantedBy=multi-user.target
EOF

#!/usr/bin/env bash

source="$1"

cat <<EOF
[Unit]
Description=Timer to start ${source}-beeminder-reporter.service

[Timer]
Persistent=true
OnCalendar=*-*-* *:58:00
AccuracySec=30s

[Install]
WantedBy=timers.target
EOF

import got, { Options, SearchParameters, CancelableRequest, Method } from 'got'
import { ok, err, mapResult, Result as BaseResult, unwrap } from '../util'

export class BeeminderApiException extends Error {}

export type UnixSeconds = number
export type UserResource = {
  username: string
  timezone: string
  updated_at: UnixSeconds
  goals: string[]
  deadbeat: boolean
  urgency_load: number
}

export type GeneralRoadSegment =
  | [UnixSeconds, number, null]
  | [UnixSeconds, null, number]
  | [null, number, number]
export type USD = number
export type HexString = string
export type GoalResourceWithoutDatapoints = {
  [P in keyof GoalResource as Exclude<P, 'datapoints'>]: GoalResource[P]
}
export type GoalResource = {
  slug: string
  updated_at: UnixSeconds
  title: string
  fineprint: string
  yaxis: string
  goaldate: UnixSeconds
  goalval: number
  rate: number
  runits: 'y' | 'm' | 'w' | 'd' | 'h'
  svg_url: string
  graph_url: string
  thumb_url: string
  autodata: string | null
  goal_type:
    | 'hustler'
    | 'biker'
    | 'fatloser'
    | 'gainer'
    | 'inboxer'
    | 'drinker'
    | 'custom'
  losedate: UnixSeconds
  queued: boolean
  secret: boolean
  datapublic: boolean
  datapoints: Array<DatapointResource>
  numpts: number
  pledge: USD
  initday: UnixSeconds
  initval: number
  curday: UnixSeconds
  curval: number
  lastday: UnixSeconds
  yaw: -1 | 1
  dir: -1 | 1
  mathishard: [UnixSeconds, number, number]
  limsum: string
  kyoom: boolean
  odom: boolean
  aggday: 'min' | 'max' | 'mean'
  steppy: boolean
  rosy: boolean
  movingav: boolean
  aura: boolean
  frozen: boolean
  won: boolean
  lost: boolean
  maxflux: number
  contract: { amount: USD; stepdown_at: UnixSeconds | null }
  road: Array<GeneralRoadSegment>
  roadall: [
    [UnixSeconds, number, null],
    ...Array<GeneralRoadSegment>,
    [UnixSeconds, number, number]
  ]
  fullroad: Array<[UnixSeconds, number, number]>
  rah: number
  delta: number
  safebuf: number
  safebump: number
  id: HexString
  callback_url: string
  deadline: number
  leadtime: number
  alertstart: number
  plotall: boolean
  last_datapoint: DatapointResource
  integery: boolean
  gunits: string
  hhmmformat: boolean
  todayta: boolean
  weekends_off: boolean
  tmin: string
  tmax: string
  tags: Array<string>
}

export type GoalUpdateObject = Partial<{
  [P in keyof GoalResource as Exclude<
    P,
    'goal_type' | 'goaldate' | 'goalval' | 'rate'
  >]: GoalResource[P]
}>

export type DatapointResource = {
  id: string
  timestamp: UnixSeconds
  daystamp: string
  value: number
  comment: string
  updated_at: UnixSeconds
  requestid: string
}

type GetOptions = {
  [Property in keyof Options as Exclude<
    Property,
    'searchParams' | 'url'
  >]: Options[Property]
} & {
  searchParams: SearchParameters
}

type Result<T> = Promise<BaseResult<T, string>>

export class BeeminderAdapter {
  apiToken: string
  constructor(apiToken: string) {
    this.apiToken = apiToken
  }

  async get<T = unknown>(path: string, options: Partial<GetOptions> = {}): Result<T> {
    const args = {
      url: `https://beeminder.com/api/v1/${path.replace(/^\/+/, '')}`,
      ...options,
    }
    args.searchParams = { ...args.searchParams, auth_token: this.apiToken }
    try {
      return ok((await (got(args) as CancelableRequest).json()) as unknown as T)
    } catch (e) {
      return err(
        `Error running GET ${path} with options ${JSON.stringify(options)}: ${
          e instanceof Error ? e.message : 'N/A'
        }`
      )
    }
  }

  async put<T = unknown>(path: string, options: Partial<Options>): Result<T> {
    const args = {
      url: `https://beeminder.com/api/v1/${path.replace(/^\/+/, '')}`,
      method: 'PUT' as Method,
      json: {},
      ...options,
    }
    args.json = { ...(args.json as object), auth_token: this.apiToken }
    try {
      return ok(await (got(args) as CancelableRequest).json())
    } catch (e) {
      return err(
        `Error running ${args.method} ${path} with options ${JSON.stringify(
          options
        )}: ${e instanceof Error ? e.message : 'N/A'}`
      )
    }
  }

  /********
   * USER RESOURCE
   ********/
  async getUser(): Result<UserResource> {
    return await this.get<UserResource>('/users/me.json')
  }

  async getGoal(
    slug: string,
    searchParams = { datapoints: false }
  ): Result<GoalResource | GoalResourceWithoutDatapoints> {
    const resp = await this.get(`/users/me/goals/${slug}.json`, { searchParams })

    if (searchParams.datapoints) return mapResult(resp, (x) => x as GoalResource)

    return mapResult(resp, (x) => x as GoalResourceWithoutDatapoints)
  }

  async getAllGoals(): Result<Array<GoalResource>> {
    return await this.get<Array<GoalResource>>(`/users/me/goals.json`)
  }

  async updateGoal(slug: string, json: GoalUpdateObject): Result<GoalResource> {
    return await this.put<GoalResource>(`/users/me/goals/${slug}.json`, { json })
  }

  async getLastDatapoint(slug: string): Result<DatapointResource> {
    const resp = await this.get<Array<DatapointResource>>(
      `/users/me/goals/${slug}/datapoints.json`
    )

    return mapResult(resp, (datapoints) => datapoints[0])
  }

  async addDatapoint(slug: string, value: number, comment?: string): Result<void> {
    return await this.put(`/users/me/goals/${slug}/datapoints.json`, {
      method: 'POST' as Method,
      json: { value, comment },
    })
  }
}

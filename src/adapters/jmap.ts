import got from 'got'
import { Result as BaseResult, ok, err } from '../util'

type Result<T> = Promise<BaseResult<T, string>>

export class JmapApiException extends Error {
  constructor(message: string) {
    super(message)
    Object.setPrototypeOf(this, JmapApiException.prototype)
  }
}

type BasicAuth = { user: string; pass: string }
type BearerAuth = { token: string }
export type JmapAuth = BasicAuth | BearerAuth
const authorizationHeader = (auth: JmapAuth): string => {
  if ('pass' in auth) {
    const token = Buffer.from(`${auth.user}:${auth.pass}`).toString('base64')
    return `Basic ${token}`
  }
  return `Bearer ${auth.token}`
}

export class JmapSession {
  hostname: string
  constructor(hostname: string) {
    this.hostname = hostname
  }

  async authenticate(auth: BasicAuth | BearerAuth): Result<AuthenticatedJmapSession> {
    try {
      const authHeader = authorizationHeader(auth)
      const response = (await got({
        url: `https://${this.hostname}/.well-known/jmap`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: authHeader,
        },
      }).json()) as {
        apiUrl: string
        primaryAccounts: { 'urn:ietf:params:jmap:mail': string }
      }

      const { apiUrl, primaryAccounts } = response
      const accountId = primaryAccounts['urn:ietf:params:jmap:mail']

      return ok(new AuthenticatedJmapSession(authHeader, apiUrl, accountId))
    } catch (e) {
      const user = 'user' in auth && auth.user ? `user [${auth.user}]` : 'user'
      return err(`Failure authenticating ${user} for jmap session`)
    }
  }
}

export class AuthenticatedJmapSession {
  authHeader: string
  apiUrl: string
  accountId: string
  constructor(authHeader: string, apiUrl: string, accountId: string) {
    this.authHeader = authHeader
    this.apiUrl = apiUrl
    this.accountId = accountId
  }

  async mailCount(): Result<number> {
    try {
      const response = (await got({
        url: this.apiUrl,
        method: 'POST',
        headers: {
          Authorization: this.authHeader,
          'Content-Type': 'application/json',
        },
        json: {
          using: ['urn:ietf:params:jmap:core', 'urn:ietf:params:jmap:mail'],
          methodCalls: [
            [
              'Mailbox/query',
              {
                accountId: this.accountId,
                filter: { role: 'inbox', hasAnyRole: true },
              },
              'inbox',
            ],
            [
              'Mailbox/get',
              {
                accountId: this.accountId,
                properties: ['totalEmails'],
                '#ids': {
                  resultOf: 'inbox',
                  name: 'Mailbox/query',
                  path: '/ids/*',
                },
              },
              'mailcount',
            ],
          ],
        },
      }).json()) as {
        methodResponses: [
          ['Mailbox/query', unknown],
          ['Mailbox/get', { list: [{ totalEmails: number }] }]
        ]
      }

      return ok(response.methodResponses[1][1].list[0].totalEmails)
    } catch (e) {
      console.error(e)
      return err('Failure getting inbox count')
    }
  }
}

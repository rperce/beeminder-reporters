declare module 'puppeteer-core' {
  const PUPPETEER_REVISIONS: {
    chromium: string
  }
}

import puppeteer, { BrowserFetcher, Browser, PUPPETEER_REVISIONS } from 'puppeteer-core'
import { ok, err, Result } from '../util'

type Args = {
  email: string
  password: string
  browser: Browser
}
export type Book = {
  title: string | null
  author: string | null
  readPages: number
  totalPages: number
}

export const loadChromium = async (
  path = '/var/cache/puppeteer'
): Promise<Result<Browser, string>> => {
  const browserFetcher = new BrowserFetcher({ path })
  const revisionInfo = await browserFetcher.download(PUPPETEER_REVISIONS.chromium)

  if (revisionInfo === undefined)
    return err(`Could not download chromium revision ${PUPPETEER_REVISIONS.chromium}`)

  return ok(
    await puppeteer.launch({
      executablePath: revisionInfo.executablePath,
    })
  )
}
export const loadBooksFromStorygraph = async ({
  email,
  password,
  browser,
}: Args): Promise<Result<Array<Book>, string>> => {
  try {
    const page = await browser.newPage()

    await page.goto('https://app.thestorygraph.com/users/sign_in')

    await page.type('#user_email', email)
    await page.type('#user_password', password)
    await page.click('#sign-in-btn')

    const viewCurrentlyReadingSelector = 'a[href="/currently-reading/rperce"]'
    await page.waitForSelector(viewCurrentlyReadingSelector)
    await page.goto('https://app.thestorygraph.com/currently-reading/rperce')

    const bookSectionsSelector = '.book-pane-content'
    await page.waitForSelector(bookSectionsSelector)
    await page.click('#close-cookies-popup')

    const books = await page.evaluate((bookSectionsSelector) => {
      const extractReadPages = (bookSection: Element): number => {
        const input = bookSection.querySelector('input.read-status-progress-number')
        const value = input?.getAttribute('placeholder')

        return parseInt(value || 'NaN')
      }

      const extractTotalPages = (bookSection: Element): number => {
        const paragraph = bookSection.querySelector('.standard-pane > p:last-of-type')
        const pageMatch = paragraph?.textContent?.match(/(\d+) pages/)
        const pageNumber = (pageMatch || ['', undefined])[1]

        return parseInt(pageNumber || 'NaN')
      }

      const extractText = (section: Element | null, selector: string): string | null =>
        section?.querySelector(selector)?.textContent?.trim() || null

      return Array.from(document.querySelectorAll(bookSectionsSelector)).map(
        (bookSection) => {
          const header = bookSection.querySelector('.book-title-author-and-series')
          const title = extractText(header, 'h3')
          const author = extractText(header, 'p:last-child')
          const readPages = extractReadPages(bookSection)
          const totalPages = extractTotalPages(bookSection)

          return { title, author, readPages, totalPages }
        }
      )
    }, bookSectionsSelector)

    await browser.close()

    return ok(books)
  } catch (e) {
    console.error(e)
    return err(
      `Failure getting books from storygraph: ${
        e instanceof Error ? e.message : 'unknown'
      }`
    )
  }
}

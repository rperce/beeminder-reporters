import fs from 'fs'
import TOML from '@iarna/toml'
import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import _merge from 'lodash.merge'
import { reporterVersions } from '../package.json'
export const loadConf = <T extends object>(
  versionKey: keyof typeof reporterVersions,
  overrides: T
): typeof overrides => {
  const args = yargs(hideBin(process.argv))
    .version(reporterVersions[versionKey])
    .options({
      config: {
        alias: 'c',
        type: 'string',
        default: '/etc/conf.d/beeminder-reporters.toml',
        describe: 'Path to config file',
      },
    })
    .usage('Usage: $0 [--config <config>]')
    .parseSync()

  const conf = TOML.parse(fs.readFileSync(args.config, 'utf-8'))

  const merged = _merge(conf, overrides)

  return merged
}

import { JmapSession, JmapAuth } from './adapters/jmap'
import { BeeminderAdapter } from './adapters/beeminder'
import { die, unwrap } from './util'
import { loadConf } from './config'

type UnixTime = number
type Seconds = number
function timeDelta(timestamp: UnixTime): Seconds {
  const now = Math.floor(Date.now() / 1000)
  return now - timestamp
}

const loadAuth = () => {
  const env = process.env
  const conf = loadConf('jmap', {
    beeminder: {
      token: env.BEEMINDER_TOKEN,
    },
    jmap: {
      user: env.JMAP_USER,
      pass: env.JMAP_PASS,
      token: env.JMAP_TOKEN,
    },
  })
  const jmap = conf.jmap

  if (!!jmap.user !== !!jmap.pass) {
    die(
      `Error: JMAP_USER and JMAP_PASS must be both set or both unset, but user is (${process.env.JMAP_USER}) and pass is (${process.env.JMAP_PASS})`
    )
  }

  if (!!jmap.user === !!jmap.token) {
    die('Error: specify either (JMAP_USER and JMAP_PASS) or JMAP_TOKEN.')
  }

  // now we know that _either_ jmap_user and jmap_pass are set,
  // _or_ JMAP_TOKEN is set, so we convince typescript of that:
  let jmap_auth: JmapAuth
  if (!!jmap.user && !!jmap.pass) {
    jmap_auth = { user: jmap.user, pass: jmap.pass }
  } else {
    jmap_auth = { token: jmap.token as string }
  }

  if (!conf.beeminder.token) {
    die('Error: BEEMINDER_TOKEN is blank.')
  }

  return {
    beeminder: conf.beeminder.token!,
    jmap: jmap_auth,
  }
}

;(async () => {
  try {
    const auth = loadAuth()

    const jmap = unwrap(
      await new JmapSession('api.fastmail.com').authenticate(auth.jmap)
    )
    const bmndr = new BeeminderAdapter(auth.beeminder)

    const inboxCount = unwrap(await jmap.mailCount())
    const lastDatapoint = unwrap(await bmndr.getLastDatapoint('inbox-zero'))

    const addDatapoint = async () =>
      void (await bmndr.addDatapoint('inbox-zero', inboxCount))

    if (inboxCount !== lastDatapoint.value) {
      await addDatapoint()
      console.log('Submitted datapoint with new inbox count', inboxCount)
    } else if (timeDelta(lastDatapoint.timestamp) > 6 * 60 * 60) {
      await addDatapoint()
      console.log('Last datapoint is more than 6h old; resubmitted', inboxCount)
    } else {
      console.log(`Last datapoint already ${inboxCount}: took no action`)
    }
  } catch (e) {
    console.warn('Error:', e)
  }
})()

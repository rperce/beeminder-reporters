import { loadChromium, loadBooksFromStorygraph } from './adapters/storygraph'
import { BeeminderAdapter } from './adapters/beeminder'
import { loadConf } from './config'
import { unwrap, die } from './util'

const loadAuth = () => {
  const env = process.env
  const conf = loadConf('storygraph', {
    beeminder: {
      token: env.BEEMINDER_TOKEN,
    },
    storygraph: {
      user: env.STORYGRAPH_USER,
      pass: env.STORYGRAPH_PASS,
      puppeteer_location: env.PUPPETEER_RUNTIME_LOCATION,
    },
  })

  if (!conf.storygraph.user || !conf.storygraph.pass) {
    die('Error: STORYGRAPH_USER and STORYGRAPH_PASS must both be set')
  }

  if (!conf.beeminder.token) {
    die('Error: BEEMINDER_TOKEN must be set')
  }

  return {
    beeminder: conf.beeminder.token!,
    storygraph: {
      email: conf.storygraph.user!,
      password: conf.storygraph.pass!,
      path: conf.storygraph.puppeteer_location,
    },
  }
}

;(async () => {
  const auth = loadAuth()
  console.log('Ensuring chromium installed...')
  const browser = unwrap(await loadChromium(auth.storygraph.path))

  console.log('Loading books from storygraph...')
  const books = unwrap(
    await loadBooksFromStorygraph({
      email: auth.storygraph.email,
      password: auth.storygraph.password,
      browser,
    })
  )

  console.log('Loading goals from beeminder...')
  const bmndr = new BeeminderAdapter(auth.beeminder)
  const goals = unwrap(await bmndr.getAllGoals())

  console.log('Updating goals...')
  for (const book of books) {
    if (!book.title || !book.author) {
      console.log(`Warning: missing title or author for ${JSON.stringify(book)}`)
      continue
    }
    const matches = goals.filter((goal) => {
      const search = goal.title + goal.fineprint
      return search.includes(book.title!) && search.includes(book.author!)
    })

    if (matches.length === 0) {
      console.log(`Warning: no matching goal found for ${book.title} by ${book.author}`)
      continue
    } else if (matches.length > 1) {
      console.log(
        `Warning: multiple goals found for ${book.title} by ${book.author}: ${matches
          .map((goal) => goal.slug)
          .join(', ')}`
      )
      continue
    }

    const goal = matches[0]
    console.log(`Found goal ${goal.slug} for ${book.title} by ${book.author}`)
    if (goal.goalval != book.totalPages) {
      console.log(
        `${goal.slug} goalval ${goal.goalval} does not match total page count ${book.totalPages}; adjusting...`
      )

      const roadall = goal.roadall
      roadall[roadall.length - 1][1] = book.totalPages

      unwrap(await bmndr.updateGoal(goal.slug, { roadall }))
    }

    const pages = book.readPages

    const lastDatapoint = unwrap(await bmndr.getLastDatapoint(goal.slug))
    if (lastDatapoint.value === pages) {
      console.log(`Last datapoint of ${goal.slug} already ${pages}; took no action`)
    } else {
      unwrap(
        await bmndr.addDatapoint(
          goal.slug,
          pages,
          `Created by storygraph-beeminder-reporter at ${new Date().toISOString()}`
        )
      )
      console.log(`Submitted datapoint to ${goal.slug} with new value ${pages}`)
    }
  }
})()

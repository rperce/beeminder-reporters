export type Result<T, E = Error> = { ok: true; val: T } | { ok: false; val: E }
export const ok = <T, E>(val: T): Result<T, E> => ({ ok: true, val })
export const err = <T, E>(val: E): Result<T, E> => ({ ok: false, val })
export const unwrap = <T, E>(result: Result<T, E>): T => {
  if (result.ok) {
    return result.val
  }

  throw result.val
}
export const mapResult = <T, U, E>(
  result: Result<T, E>,
  fn: (x: T) => U
): Result<U, E> => (result.ok ? ok(fn(result.val)) : result)
export const asyncMapResult = async <T, U, E>(
  result: Result<T, E>,
  fn: (x: T) => U
): Promise<Result<U, E>> => (result.ok ? ok(await fn(result.val)) : result)

export const die = (msg: string): never => {
  console.warn(msg)
  process.exit(1)
}
